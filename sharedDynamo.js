'use strict';
const AWS = require('aws-sdk');
const DDB = new AWS.DynamoDB.DocumentClient();


exports.addContact = async(contact) => {


};

exports.RemoveContact = async(contact) => {

};

exports.updateContact = async(contact) => {


};


//get the currently open contact between 2 tags
exports.getContacts = async(event) => {

};




























exports.RemoveContact = async(contact) => {
    var sql = 'DELETE FROM exposures WHERE id = $1';

    var params = [
        contact.id
    ];

    return await this.executeQuery(sql, params);
};

exports.updateContact = async(contact) => {

    var sql = 'UPDATE exposures SET event_start=$1, event_end=$2, range=$3, duration=$4, updated_at=$5 WHERE id =$6';

    var params = [
        new Date(Number(contact.event_start) * 1000), //in seconds
        new Date(Number(contact.event_end) * 1000), //in seconds
        contact.range,
        contact.duration,
        new Date(),
        contact.id
    ];

    return await this.executeQuery(sql, params);
};


//get the currently open contact between 2 tags
exports.getContacts = async(event) => {
    var sql = "SELECT * FROM exposures WHERE \
    ( \
        (tag_a = $1 AND tag_b = $2) OR \
        (tag_a = $2 AND tag_b = $1) \
    ) AND \
    ( \
      (event_start <= $3 AND event_end >= $3) OR \
      (event_start >= $3 AND event_end <= $4) OR \
      (event_start <= $4 AND event_end >= $4) \
     ) AND \
     range = $5 \
     ORDER BY event_start ASC";

    var params = [
        event.tag_a,
        event.tag_b,
        new Date((Number(event.event_start) - Number(process.env.INTERPOLATE)) * 1000), //in seconds
        new Date((Number(event.event_end) + Number(process.env.INTERPOLATE)) * 1000), //in seconds,
        event.range
    ];

    return await this.executeQuery(sql, params);
};
