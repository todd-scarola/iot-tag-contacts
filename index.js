'use strict';

//inboud message is an SQS trigger
exports.handler = async(event) => {

    var responses = [];

    //need to figure out which property to ID SQS messages
    if (event.hasOwnProperty('Records'))
    //SQS event
    {
        for (const record of event.Records) {
            responses.push(await this.processMessage(JSON.parse(record.body)));
        }
        return responses;
    }
    else
    //normal JSON request
    {
        return await this.processMessage(event);
    }
};



exports.processMessage = async(event) => {

    //get processing function
    var helper = null;
    if (event.hasOwnProperty('messageType')) {
        try {
            helper = require('./messages/' + event.messageType + '/main.js');
        }
        catch (error) {
            console.error(error);
        }
    }

    if (helper) {
        var message = await helper.processData(event);
        if (message) {
            return message;
        }
    }
};
