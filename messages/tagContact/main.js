'use strict';
const PG = require('../../sharedPostgres.js');

//main BL for aggregating a contact into an exposure
exports.processData = async(event) => {

   //get any existing contacts to factor
   var existing_contacts = await PG.getContacts(event);

   //no existing, add new contact
   if (existing_contacts.rowCount == 0) {
      console.log('adding new contact');
      await PG.addContact(event);
   }
   //selectively update existing contact
   else if (existing_contacts.rowCount == 1) {
      let contact = existing_contacts.rows[0];

      //convert dates back to epoch seconds
      contact.event_start = new Date(contact.event_start).getTime() / 1000;
      contact.event_end = new Date(contact.event_end).getTime() / 1000;

      if (event.event_start < contact.event_start || event.event_end > contact.event_end) {

         console.log('extending contact %j', contact);

         //pick earliest start date
         if (event.event_start < contact.event_start) {
            console.log('updating start date');
            contact.event_start = event.event_start;
         }

         //pick latest end date
         if (event.event_end > contact.event_end) {
            console.log('updating end date');
            contact.event_end = event.event_end;
         }

         //compute the new duration
         contact.duration = contact.event_end - contact.event_start;

         //update existing contact
         console.log('updating contact %j', contact);
         await PG.updateContact(contact);
      }
      else {
         console.log('ignoring event');
      }
   }
   //merge existing contacts
   else if (existing_contacts.rowCount > 1) {

      //use earliest entry as working contact
      let base_contact = existing_contacts.rows[0];
      console.log('merging contact %j', base_contact);

      //evaluate event dates first

      //convert dates back to epoch seconds
      base_contact.event_start = new Date(base_contact.event_start).getTime() / 1000;
      base_contact.event_end = new Date(base_contact.event_end).getTime() / 1000;

      //pick earliest start date
      if (event.event_start < base_contact.event_start) {
         base_contact.event_start = event.event_start;
      }

      //pick latest end date
      if (event.event_end > base_contact.event_end) {
         base_contact.event_end = event.event_end;
      }

      //evaluate remaining contacts, start at 1 instead of 0
      for (let i = 1; i < existing_contacts.rows.length; i++) {
         let contact = existing_contacts.rows[i];

         //convert dates back to epoch seconds
         contact.event_start = new Date(contact.event_start).getTime() / 1000;
         contact.event_end = new Date(contact.event_end).getTime() / 1000;


         //pick earliest start date
         if (contact.event_start < base_contact.event_start) {
            base_contact.event_start = Number(contact.event_start);
         }

         //pick latest end date
         if (contact.event_end > base_contact.event_end) {
            base_contact.event_end = Number(contact.event_end);
         }

         //delete existing contact (need to do this 1st for possible constraint collision on tag/date/range)
         //TO DO throw error on fail or wrap in transaction would be better
         console.log('removing contact %j', contact);
         await PG.RemoveContact(contact);

      }

      //update duration
      base_contact.duration = base_contact.event_end - base_contact.event_start;

      //update base contact
      console.log('updating contact %j', base_contact);
      await PG.updateContact(base_contact);

   }

   return event;

};


/*
{
  "messageType": "tagContact",
  "gateway_id": "354616090312245",
  "tag_a": "cd71770dac99",
  "tag_b": "e62c2d5babb8",
  "event_start": 1608570155,
  "event_end": 1608570215,
  "range": 2,
  "duration": 60,
  "meta": {
    "tag_a": {
      "id": "2998",
      "mac_address": "cd71770dac99",
      "account_id": "40",
      "manufacturer": "Laird",
      "model": "BT510",
      "nickname": "",
      "box_number": "56"
    },
    "tag_b": {
      "id": "5812",
      "mac_address": "e62c2d5babb8",
      "account_id": "40",
      "manufacturer": "Laird",
      "model": "BT510",
      "nickname": null,
      "box_number": null
    },
    "user_a": {
      "user_id": "2939",
      "user_type": "User",
      "checked_in_at": "2020-11-09T17:21:05.317Z",
      "checked_out_at": null
    },
    "user_b": {
      "user_id": "5064",
      "user_type": "User",
      "checked_in_at": "2020-12-16T17:52:34.346Z",
      "checked_out_at": null
    }
  }
}
*/
