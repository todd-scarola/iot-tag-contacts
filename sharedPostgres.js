const { Client } = require('pg');
//see this for connection string: https://node-postgres.com/features/connecting

exports.executeQuery = async(sql, params) => {
    //console.log('sql: %s params: %s', sql, params);

    //db Client
    var client = new Client();

    try {
        await client.connect();
        var result = await client.query(sql, params);
        //console.log('sql result: %s', result);
    }
    catch (error) {
        if (error.code == 23505) {
            console.log('Duplicate Key: %s, %s', sql, params);
        }
        else {
            console.error(error);
        }
    }

    //cleanup connection
    await client.end();

    return result;
};


exports.addContact = async(contact) => {

    var sql = 'INSERT INTO exposures ( tag_a, tag_b, event_start, event_end, range, duration, updated_at, user_a, user_b, user_a_type, user_b_type) \
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) \
    RETURNING id';

    var params = [
        contact.tag_a,
        contact.tag_b,
        new Date(Number(contact.event_start) * 1000), //in seconds
        new Date(Number(contact.event_end) * 1000), //in seconds
        contact.range,
        contact.duration,
        new Date(),
        ('user_a' in contact.meta ? contact.meta.user_a.user_id : null),
        ('user_b' in contact.meta ? contact.meta.user_b.user_id : null),
        ('user_a' in contact.meta ? contact.meta.user_a.user_type : null),
        ('user_b' in contact.meta ? contact.meta.user_a.user_type : null)
    ];

    return await this.executeQuery(sql, params);
};

exports.RemoveContact = async(contact) => {
    var sql = 'DELETE FROM exposures WHERE id = $1';

    var params = [
        contact.id
    ];

    return await this.executeQuery(sql, params);
};

exports.updateContact = async(contact) => {

    var sql = 'UPDATE exposures SET event_start=$1, event_end=$2, range=$3, duration=$4, updated_at=$5 WHERE id =$6';

    var params = [
        new Date(Number(contact.event_start) * 1000), //in seconds
        new Date(Number(contact.event_end) * 1000), //in seconds
        contact.range,
        contact.duration,
        new Date(),
        contact.id
    ];

    return await this.executeQuery(sql, params);
};


//get the currently open contact between 2 tags
exports.getContacts = async(event) => {
    var sql = "SELECT * FROM exposures WHERE \
    ( \
        (tag_a = $1 AND tag_b = $2) OR \
        (tag_a = $2 AND tag_b = $1) \
    ) AND \
    ( \
      (event_start <= $3 AND event_end >= $3) OR \
      (event_start >= $3 AND event_end <= $4) OR \
      (event_start <= $4 AND event_end >= $4) \
     ) AND \
     range = $5 \
     ORDER BY event_start ASC";

    var params = [
        event.tag_a,
        event.tag_b,
        new Date((Number(event.event_start) - Number(process.env.INTERPOLATE)) * 1000), //in seconds
        new Date((Number(event.event_end) + Number(process.env.INTERPOLATE)) * 1000), //in seconds,
        event.range
    ];

    return await this.executeQuery(sql, params);
};
